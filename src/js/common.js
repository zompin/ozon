/*
	Алтухов Илья
	zompin@ya.ru
*/

let registry 	= {};
let src 		= 'https://api.instagram.com/v1/users/691623/media/recent?access_token=691623.1419b97.479e4603aff24de596b1bf18891729f3&count=20';

onload = function() {
	let gallery = document.querySelector('.gallery');

	jsonp(src, function(err, obj) {
		document.querySelector('.loader').style.display = 'none';
		if (err || obj.meta.code != 200) {
			alert('Ошибка, не удалось загрузить данные');
		} else {
			obj.data.forEach(function(item) {
				gallery.appendChild(getTemplate(item));
			});
		}
	});
}

function jsonp(src, callback) {
	let s 				= document.createElement('script');
	let callbackName 	= 'cb' + Date.now();

	if (src.indexOf('?') >= 0) {
		src += '&';
	} else {
		src += '?';
	}

	src += 'callback=registry.' + callbackName;
	s.src = src;
	s.callbackName = callbackName;

	registry[callbackName] = function(data) {
		if (callback) {
			callback(null, data);
		} else {
			throw new Error('Nothing to call');
		}
	}

	s.onload = function() {
		registry[this.callbackName] = null;
	}

	s.onerror = function(err) {
		if (callback) {
			callback(err);
		} else {
			throw new Error('Nothing to call');
		}
	}

	document.body.appendChild(s);
}

function getTemplate(item) {
	let template = {
		type: 'div',
		classes: 'gallery__item-cont',
		content: {
			type: 'div',
			classes: 'gallery__item',
			content: [
				{
					type: 'div',
					classes: 'user',
					content: [
						{
							type: 'div',
							classes: 'user__avatar',
							content: {
								type: 'img',
								classes: 'user__avatar-img',
								attr: {
									src: item.user.profile_picture
								}
							}
						}, {
							type: 'div',
							classes: 'user__username',
							content: {
								type: 'a',
								classes: 'user__username-inner',
								attr: {
									href: 'https://www.instagram.com/' + item.user.username
								},
								content: item.user.username
							}
						}, {
							type: 'div',
							classes: 'user__location',
							content: {
								type: 'div',
								classes: 'user__location-inner',
								content: item.location ? item.location.name : null
							}
						}, {
							type: 'div',
							classes: 'user__updated',
							content: formatUpdated(item.created_time)
						}
					]
				}, {
					type: 'a',
					classes: 'photo photo_blur',
					attr: {
						href: item.link
					},
					content: {
						type: 'img',
						classes: 'photo__img',
						events: {
								type: 'load',
								callback: function() {
									this.classList.add('photo__img_loaded');
								}
							},
						attr: {
							src: item.images.low_resolution.url
						}
					}
				}, {
					type: 'div',
					classes: 'desc',
					content: [
						{
							type: 'div',
							classes: 'desc__likes',
							content: [
								{
									type: 'button',
									classes: 'like-button',
									events: {
												type: 'click',
												callback: function() {
													alert(item.id);
												}
											}
								}, {
									type: 'span',
									classes: 'desc__likes-amount',
									content: formatCount(item.likes.count)
								}
							]
						}, {
							type: 'div',
							classes: 'desc__text',
							content: item.caption ? item.caption.text : null
						}
					]
				}
			]
		}
	}

	return renderTemplate(template);
}

function renderTemplate(node) {
	let elem;
	let child;

	if (node instanceof Array) {
		return node.map(function(el) {
			return renderTemplate(el);
		});
	} else if (node instanceof Object) {
		elem = createElement(node);

		if (node.content) child = renderTemplate(node.content);

		if (child instanceof Array) {
			child.forEach(function(el) {
				elem.appendChild(el);
			});
		} else if (child) {
			elem.appendChild(child);
		}

		return elem;
	} else {
		return document.createTextNode(node);
	}
}

function createElement(node) {
	let elem = document.createElement(node.type);
	let prop;

	if (node.classes) elem.className = node.classes;

	if (node.events) {
		if (node.events instanceof Array) {
			node.events.forEach(event => {
				elem.addEventListener(event.type, event.callback);
			});
		} else {
			elem.addEventListener(node.events.type, node.events.callback);
		}
	}

	for (prop in node.attr) {
		if (node.attr[prop]) elem.setAttribute(prop, node.attr[prop]);
	}

	return elem;
}

function formatUpdated(time) {
	let now 		= Date.now();
	const minute 	= 60000;
	const hour 		= minute * 60;
	const day 		= hour * 24;
	const week 		= day * 7;
	const month 	= week * 4;
	const year 		= month * 12
	let update 		= '';
	time 			= now - time * 1000;

	if (time < minute) {
		update = 'сек.';
		time = Math.ceil(time / 1000);
	} else if (time < hour) {
		update = 'мин.';
		time = Math.ceil(time / minute);
	} else if (time < day) {
		update = 'час.';
		time = Math.ceil(time / hour);
	} else if (time < week) {
		update = 'дн.';
		time = Math.ceil(time / day);
	} else if (time < month * 2) {
		update = 'нед.';
		time = Math.ceil(time / week);
	} else if (time < year) {
		update = "мес."
		time = Math.ceil(time / month);
	} else {
		update = 'год.';
		time = Math.ceil(time / year);
	}

	return time + ' ' + update;
}

function formatCount(n) {
	let buff = '';
	n += '';

	for (let i = n.length - 1, j = 0; i >= 0; i--, j++) {
		if (j % 3 == 0) {
			buff = ' ' + buff;
		}

		buff = n[i] + buff;
	}

	return buff;
}